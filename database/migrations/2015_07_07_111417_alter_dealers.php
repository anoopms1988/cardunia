<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDealers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dealers', function(Blueprint $table)
		{
			$table->string( 'picture', 50 )->nullable();
            $table->string( 'facebook_link', 50 )->nullable();
            $table->string( 'twitter_link', 50 )->nullable();
            $table->string( 'linkedin_link', 50 )->nullable();
            $table->string( 'google_link', 50 )->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dealers', function(Blueprint $table)
		{
			//
		});
	}

}
