
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Edit dealer details</h4>
                            </div>
                            {!! Form::open(array('action' =>'GeneralController@updateDealerDetails','id'=>'edit_dealer_details_form')) !!}
                            <input type="hidden" name="dealer_id" value="{{$Dealer->id or ''}}">
                            <div class="modal-body">
                                <?php
echo Form::label( 'company', 'Company' );
echo Form::select( 'company', array('' => 'Select') + $companies, $Dealer->company_id, array('class' => 'form-control') );
echo '<br>';
echo Form::label( 'city', 'City' );
echo Form::select( 'city', array('' => 'Select') + $cities, $Dealer->city_id, array('class' => 'form-control') );
echo '<br>';
echo Form::label( 'address', 'Address' );
echo Form::text( 'address', $Dealer->address, array('class' => 'form-control') );
echo '<br>';
echo Form::label( 'name', 'Name' );
echo Form::text( 'name', $Dealer->name, array('class' => 'form-control') );
echo '<br>';
echo Form::label( 'phonenumber', 'Phonenumber' );
echo Form::text( 'phonenumber', $Dealer->phonenumber, array('class' => 'form-control') );
echo '<br>';
echo Form::label( 'mobilenumber', 'Mobilenumber' );
echo Form::text( 'mobilenumber', $Dealer->phonenumber, array('class' => 'form-control') );
echo '<br>';
echo Form::label( 'email', 'Email' );
echo Form::text( 'email', $Dealer->email, array('class' => 'form-control') );
echo '<br>';
echo Form::label( 'Opening hours', 'Opening hours' );
echo Form::text( 'opening_hours', $Dealer->opening_hours, array('class' => 'form-control') );
?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" name="update_dealer_details" class="btn btn-primary" value="Update">
                            </div>
                            {!! Form::close() !!}
           <script type="text/javascript">
    $(document).ready(function () {
         
         $("#edit_dealer_details_form").validate({
            rules: {
                company: "required",
                city: "required",
                address: "required",
                name: "required", 
                phonenumber: "required", 
                mobilenumber: "required", 
                email:{
                    email: true,
                    required:true
                }
            },
            messages: {
                company: "Enter company",
                city: "Enter city",
                address: "Enter address",
                name: "Enter name", 
                phonenumber: "Enter phonenumber", 
                mobilenumber: "Enter mobilenumber", 
                email:{
                    email: true,
                    required:true
                }
            }
        });
    });
</script>   